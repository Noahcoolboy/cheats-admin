const commandFiles = require('./commands')

const teamRegex = /t\:[^\:]+\:/

function adminMiddleware(player, args, next) {
    if (player.admin)
        return next(player, args)
    if (Game.cheatsAdmin.owners.includes(player.userId))
        return next(player, args)
    if (Game.cheatsAdmin.admins.includes(player.userId)) 
        return next(player, args)
}

function adminMessage(message) {
    return `[#ff0000][V3]: [#ffffff]${message}`
}

function getValueOrTrue(value) {
    return typeof value === 'undefined' || value
}

class CheatsAdmin {
    constructor() {
        this._commands = {}
        this.owners = []
        this.admins = []
        this.banned = []
        this.audit = true
        this.ownerOnly = true
        this.maxScale = 10
        this.minScale = 0.1
    }

    createCommand(commandOptions, callback) {
        const commands = [ commandOptions.name ]
            
        if (commandOptions.aliases)
            commands.push(...commandOptions.aliases)

        const cmd = Game.commands(commands, commandOptions.middleware || adminMiddleware, (caller, args) => {
            const commandData = {
                name: commandOptions.name,
                autoGetTarget: getValueOrTrue(commandOptions.autoGetTarget),
                audit: getValueOrTrue(commandOptions.audit),
                caller: caller,
                args: args,
                ownerOnly: commandOptions.ownerOnly || false,
                target: null,
                players: []
            }

            if ((this.ownerOnly && commandData.ownerOnly) && !this.isOwner(caller)) return

            if (commandData.autoGetTarget) {
                const targetArgs = this.getTarget(commandData, args)
                commandData.target = targetArgs.target
                commandData.players = targetArgs.players
            }
            callback(commandData)
        })
        this._commands[commandOptions.name] = cmd
    }
    
    adminPlayer(p) {
        this.admins.push(p.userId)
    }

    banPlayer(p) {
        this.banned.push(p.userId)
    }
    
    unadminPlayer(p) {
        this.admins.splice(this.admins.indexOf(p.userId))
    }
    
    unbanPlayer(p) {
        this.banned.splice(this.banned.indexOf(p.userId))
    }

    isOwner(p) { return this.owners.includes(p.userId) }

    isBanned(p) { return this.banned.includes(p.userId) }

    isAdmin(p) { return this.admins.includes(p.userId) }

    logAudit(commandData) {
        console.table([{ 
            Username: commandData.caller.username,
            UserId: commandData.caller.userId,
            Command: commandData.name,
            Target: commandData.target || null
        }])
    }

    deleteCommand(commandName) {
        const command = this._commands[commandName]
        if (!command) return
        command.disconnect()
        this._commands[commandName] = null
    }

    getTarget(commandData, args) {
        const targetData = {
            target: null,
            players: []
        }
        switch(args) {
            case ":me": {
                targetData.target = ':me'
                targetData.players = [ commandData.caller ]
                break
            }
            case ":all": {
                targetData.target = ':all'
                targetData.players = Game.players
                break
            }
            case ":others": {
                targetData.target = ':others'
                targetData.players = Game.players.filter(p => p !== commandData.caller)
                break
            }
            case ":random": {
                const randomIndex = Math.floor(Math.random() * Game.players.length)
                const victim = Game.players[randomIndex]
                targetData.target = victim.username
                targetData.players = [ victim ]
                break
            }
            case ":admins": {
                targetData.target = ':admins'
                targetData.players = Game.players.filter(p => this.isAdmin(p))
                break
            }
            case ":nonadmins": {
                targetData.target = ':nonadmins'
                targetData.players = Game.players.filter(p => !this.isAdmin(p))
                break
            }
            case String(args.match(teamRegex)): {
                const match = String(args.match(teamRegex)).toLowerCase()
                const teamName = match.split(":")[1]

                if (!world.teams.length || !world.teams.find(t => t.name.toLowerCase().indexOf(teamName) === 0)) {
                    commandData.caller.message(adminMessage("Team was not found."))
                    break
                }

                const teamMembers = Game.players.filter(p => p.team && p.team.name.toLowerCase().indexOf(teamName) === 0)
                targetData.target = "Team: " + teamName
                targetData.players = teamMembers
                
                break
            }
            default: {
                args = args.toLowerCase()
                for (let player of Game.players) {
                    if (player.username.toLowerCase().indexOf(args) === 0) {
                        const victim = Game.players.find(v => v.username === player.username)
                        targetData.target = victim.username
                        targetData.players = [ victim ]
                    }
                }
            }
        }

        if (this.audit && commandData.audit)
            this.logAudit({
                name: commandData.name,
                caller: commandData.caller,
                target: targetData.target
            })

        if (!targetData.target)
            commandData.caller.message(adminMessage("User or value is not valid."))

        return targetData
    }

}

Game.setMaxListeners(100)

Game.cheatsAdmin = new CheatsAdmin()

commandFiles.forEach((script) => require('./commands/' + script))

Game.on('initialSpawn', (p) => {
    p.message(adminMessage("This server uses Cheats' V3 Commands."))

    if (Game.cheatsAdmin.isBanned(p))
        return p.kick("You are banned from the server!")

    if (Game.cheatsAdmin.isAdmin(p) || Game.cheatsAdmin.isOwner(p))
        return p.centerPrint(adminMessage("You are an admin!"), 3)
})