const Jail = require('../class/jail')

const userValue = /([^"]+)(?:\"([^\"]+)\"+)?/

const cheatsAdmin = Game.cheatsAdmin

function adminMessage(message) {
    return `[#ff0000][V3]: [#ffffff]${message}`
}

cheatsAdmin.createCommand({
    name: 'kick',
    autoGetTarget: false,
    ownerOnly: true
}, (cmd) => {
    const match = cmd.args.match(userValue)
    if (!match || !match[1]) return

    const user = match[1].trim()
    const reason = match[2]

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((player) => {
        if (cheatsAdmin.isOwner(player)) return
        player.kick(reason)
    })
})

cheatsAdmin.createCommand({
    name: 'ban',
    ownerOnly: true,
    autoGetTarget: false
}, (cmd) => {
    const match = cmd.args.match(userValue)
    if (!match || !match[1]) return

    const user = match[1].trim()
    const reason = match[2]

    const target = cheatsAdmin.getTarget(cmd, user)
    target.players.forEach((victim) => {
        if (cheatsAdmin.isOwner(victim)) return
        cheatsAdmin.banPlayer(victim)
        cmd.caller.message(adminMessage(`You banned [#ff0000]${victim.username}[#ffffff].` ))
        victim.kick(reason)
    })
})

cheatsAdmin.createCommand({
    name: 'unban',
    autoGetTarget: false
}, (cmd) => {
    const id = Number(cmd.args)
    if (isNaN(id)) return

    if (!cheatsAdmin.banned.includes(id))
        return cmd.caller.message(adminMessage("This player is not banned."))
        
    cheatsAdmin.banned.splice(cheatsAdmin.banned.indexOf(id))

    cmd.caller.message(adminMessage("Unbanned userId: " + id))
})

cheatsAdmin.createCommand({
    name: 'vchat',
    autoGetTarget: false
}, (cmd) => {
    const admins = Game.players.filter(p => cheatsAdmin.isAdmin(p))
    cheatsAdmin.logAudit(cmd)
    admins.forEach((admin) => {
        admin.message(`[#ff0000][${cmd.caller.username}]: [#ffffff]${cmd.args}`)
    })
})

cheatsAdmin.createCommand({
    name: 'broadcast',
    aliases: ['b'],
    autoGetTarget: false
}, (cmd) => {
    cheatsAdmin.logAudit(cmd)
    Game.topPrintAll(`[#ff0000]${cmd.caller.username}: [#ffffff]${cmd.args}`, 5)
})

cheatsAdmin.createCommand({ name: 'mute' }, (cmd) => {
    cmd.players.forEach((victim) => {
        (victim.muted) ? 
            cmd.caller.message(adminMessage(`You unmuted ${victim.username}.`)) : 
            cmd.caller.message(adminMessage(`You muted ${victim.username}`));
        victim.muted = !victim.muted;
    })
})

cheatsAdmin.createCommand({ name: 'loopkill', ownerOnly: true }, (cmd) => {
    cmd.players.forEach((p) => {
        p.loopkill = !p.loopkill
        const loop = p.setInterval(() => {
            if (!p.loopkill) return clearInterval(loop)
            if (p.alive) p.kill()
        }, 100)
    })
})

cheatsAdmin.createCommand({ name: 'jail' }, (cmd) => {
    cmd.players.forEach((p) => {
        if (p.jail) return
        const jail = new Jail(p)
        jail.imprison()
    })
})

cheatsAdmin.createCommand({ name: 'unjail', aliases: ['free'] }, (cmd) => {
    cmd.players.forEach((p) => {
        if (!p.jail) return
        p.jail.free()
    })
})

Game.on('playerLeave', (p) => {
    return p.jail && p.jail.free()
})